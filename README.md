#  CNN

## Project Structure

```
.
├── CNN
│   ├── CNN
│   │   ├── Models        
│   │   ├── Services       # Contains all application services.
│   │   ├── Scenes         # Contains all application views and viewModels.
│   │   └── Support        # Contains Helper classes, extensions and protocols.
└── Pods
```

## Architecture description

```
├── Scenes
|    ├── ViewModel
|    └── View
|        ├── Cell
|        └── Control
|
├── Support Files
```

## Dependencies 
- Alamofire 
- KingFisher
- Firebase Analytics
- Fabric

## Requirements To Run Project
- Xcode version 10.2.1.
- Cocoapods  (To install project dependencies).

## Steps To Run Project

- Open `Terminal` and type `sudo gem install cocoapods` (this may take a couple of minutes).
- Once the installation is done. Open `CNN.xcworkspace` file with Xcode. 
- Choose a preferred simulator from the top left corner and hit run.


## Features: 
- The app uses Alamofire 5 to handle network calls with Native codable for decoding JSON Data.
- Specific event are being sent to Firebase console for analytics.
-  The app gets news data from different sources using `newsapi.org`. 
- In each tab bar screen, there is are segmented controllers to select news for a specific country based on the tab (category) selected.
- The content in news details is truncated because the API doesn't have the full right to display all the article content. Due to this, I created a button to open the article link in Safari.
- I created a Native tab gesture recognizer to mimic a swipe to switch between tab bar items. 

## Note: 
- Newsapi.org schema is the same with each article endpoint, Due to this, and to avoid duplicated code as much as possible I created `BaseViewController` which holds the UI structure for the 3 tab bar items and 3 segmented controllers.   
