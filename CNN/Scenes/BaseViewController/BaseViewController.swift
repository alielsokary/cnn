//
//  BaseViewController.swift
//  CNN
//
//  Created by Ali on 8/27/19.
//  Copyright © 2019 mag. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

	@IBOutlet weak var headlinesTableView: UITableView!
	@IBOutlet weak var segmentedControl: UISegmentedControl!

	 let CellIdentifier = "NewsTableViewCell"
	 let NewsDetailsVCIdentifier = "NewsDetailsViewController"
	 let cellHeight: CGFloat = 300.0
	 var newsViewModels = [NewsViewModel]()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		configureNewsTableView()
		configureSwibeGestureRecognizer()
	}

// MARK: - Configuration

	 func configureSwibeGestureRecognizer() {
		let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipeGesture))
		swipeRight.direction = .right
		self.view.addGestureRecognizer(swipeRight)
		
		let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipeGesture))
		swipeLeft.direction = .left
		self.view.addGestureRecognizer(swipeLeft)
	}
	
	@objc func handleSwipeGesture(_ gesture: UISwipeGestureRecognizer) {
		guard let tabBarController = tabBarController, let viewControllers = tabBarController.viewControllers else { return }
		let tabs = viewControllers.count
		if gesture.direction == .left {
			if (tabBarController.selectedIndex) < tabs {
				tabBarController.selectedIndex += 1
			}
		} else if gesture.direction == .right {
			if (tabBarController.selectedIndex) > 0 {
				tabBarController.selectedIndex -= 1
			}
		}
	}

	 func configureNewsTableView() {
		headlinesTableView.dataSource = self
		headlinesTableView.delegate = self
		headlinesTableView.register(UINib.init(nibName: CellIdentifier, bundle: nil), forCellReuseIdentifier: CellIdentifier)
	}
	
}

// MARK: - UITableViewDataSource

extension BaseViewController: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return newsViewModels.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier, for: indexPath) as? NewsTableViewCell else { return UITableViewCell() }
		cell.newsViewModel = newsViewModels[indexPath.row]
		return cell
	}
}

// MARK: - UITableViewDelegate

extension BaseViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return cellHeight
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if let indexPath  = tableView.indexPathForSelectedRow {
			let newsDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: NewsDetailsVCIdentifier) as! NewsDetailsViewController
			
			newsDetailsViewController.newsTitleData = newsViewModels[indexPath.row].title
			newsDetailsViewController.urlImageData = newsViewModels[indexPath.item].imageURL
			newsDetailsViewController.newsDateData = newsViewModels[indexPath.item].newsdate
			newsDetailsViewController.newsSourceData = newsViewModels[indexPath.item].newsSource
			newsDetailsViewController.newsContentData = newsViewModels[indexPath.item].newsContent
			newsDetailsViewController.contentURL = newsViewModels[indexPath.row].contentUrl
			self.navigationController?.pushViewController(newsDetailsViewController, animated: true)
		}
	}
}
