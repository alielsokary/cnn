//
//  NewsTableViewCell.swift
//  CNN
//
//  Created by Ali on 8/27/19.
//  Copyright © 2019 mag. All rights reserved.
//

import UIKit
import Kingfisher

class NewsTableViewCell: UITableViewCell {
	
	@IBOutlet weak var footerView: UIView!
	@IBOutlet weak var newsTitle: UILabel!
	@IBOutlet weak var newsImage: UIImageView!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		backgroundColor = .clear
		selectionStyle = UITableViewCell.SelectionStyle.none
		newsImage.layer.cornerRadius = 8.0
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		footerView.clipsToBounds = true
		footerView.layer.cornerRadius = 8.0
		footerView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
	}
	
	var newsViewModel: NewsViewModel! {
		didSet {
			newsTitle.text = newsViewModel.title
			newsImage.kf.setImage(with: URL(string: newsViewModel.imageURL), placeholder: UIImage(named: "cnn-placeholder.png"))
		}
	}
}
