//
//  NewsViewModel.swift
//  CNN
//
//  Created by Ali on 8/27/19.
//  Copyright © 2019 mag. All rights reserved.
//

import Foundation

struct NewsViewModel {
	
	let title: String
	let imageURL: String
	let newsSource: String
	let newsdate: String
	let newsContent: String
	let contentUrl: String
	
	init(article: Article) {
		self.title = article.title ?? ""
		self.imageURL = article.urlToImage ?? ""
		self.newsSource = article.source.name ?? ""
		self.newsdate = article.publishedAt
		self.newsContent = article.content ?? "No Available Content"
		self.contentUrl = article.url ?? ""
	}
}
