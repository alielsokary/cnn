//
//  NewsDetailsViewController.swift
//  CNN
//
//  Created by Ali on 8/27/19.
//  Copyright © 2019 mag. All rights reserved.
//

import UIKit
import Kingfisher
import SafariServices
import Firebase

class NewsDetailsViewController: UIViewController {
	
	@IBOutlet weak var newsTitle: UILabel!
	@IBOutlet weak var newsImage: UIImageView!
	@IBOutlet weak var newsSource: UILabel!
	@IBOutlet weak var newsDate: UILabel!
	@IBOutlet weak var newsContent: UILabel!
	
	var newsTitleData = String()
	var urlImageData = String()
	var newsDateData = String()
	var newsSourceData = String()
	var newsContentData = String()
	var contentURL = String()
	
	override func viewDidLoad() {
        super.viewDidLoad()
        configureViewData()
		configureSourceLinkButton()
		Analytics.logEvent(AnalyticsKeys.news_details_screen.rawValue, parameters: nil)
    }
	
	fileprivate func configureViewData() {
		newsTitle.text = newsTitleData
		newsImage.kf.setImage(with: URL(string: urlImageData), placeholder: UIImage(named: "cnn-placeholder.png"))
		newsDate.text = HelperMethods.formatDate(date: newsDateData)
		newsSource.text = newsSourceData
		newsContent.text = newsContentData
	}
	
	fileprivate func configureSourceLinkButton() {
		let gesture = UITapGestureRecognizer(target: self, action: #selector(userTappedOnLink))
		newsSource.isUserInteractionEnabled = true
		newsSource.addGestureRecognizer(gesture)
	}
	
	@objc func userTappedOnLink() {
		Analytics.logEvent(AnalyticsKeys.resource_link_clicked.rawValue, parameters: nil)
		guard let url = URL(string: contentURL) else { return }
		let safaruvc = SFSafariViewController(url: url)
		present(safaruvc, animated: true, completion: nil)
	}

}
