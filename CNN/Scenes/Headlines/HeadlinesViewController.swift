//
//  HeadlinesViewController.swift
//  CNN
//
//  Created by Ali on 8/26/19.
//  Copyright © 2019 mag. All rights reserved.
//

import UIKit
import Firebase

class HeadlinesViewController: BaseViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		getTopHeadlines()
		configureViewUI()
		Analytics.logEvent(AnalyticsKeys.home_screen.rawValue, parameters: nil)
	}

	fileprivate func configureViewUI() {
		title = "Headlines"
	}
	
	fileprivate func getTopHeadlines(country: String? = Countries.usa.rawValue, category: String? = Categories.general.rawValue) {
		APIClient.getTopHeadlines(parameters: [K.APIParameterKey.country: country!, K.APIParameterKey.category: category!]) { [weak self] result in
			switch result {
			case .success(let headlines):
				self?.newsViewModels = headlines.articles.map { return NewsViewModel(article: $0) }
				self?.headlinesTableView.reloadData()				
			case .failure(let error):
				self?.presentAlert(withTitle: "Error", message: error.localizedDescription)
			}
		}
	}

	@IBAction func segmentIndexChanged(_ sender: Any) {
		switch segmentedControl.selectedSegmentIndex {
		case 0:
			getTopHeadlines(country: Countries.usa.rawValue)
		case 1:
			print("sadaldakdjnajd")
			getTopHeadlines(country: Countries.germany.rawValue)
		case 2:
			getTopHeadlines(country: Countries.france.rawValue)
		case 3:
			getTopHeadlines(country: Countries.italy.rawValue)
		case 4:
			getTopHeadlines(country: Countries.russia.rawValue)
			
		default:
			break;
		}
	}
	
}
