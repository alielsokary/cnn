//
//  APIClient.swift
//  CNN
//
//  Created by Ali on 8/27/19.
//  Copyright © 2019 mag. All rights reserved.
//

import Alamofire

class APIClient {
	@discardableResult private static func request<T:Decodable>(route:APIRouter, decoder: JSONDecoder = JSONDecoder(), completion: @escaping (AFResult<T>)->Void) -> DataRequest {
		return AF.request(route)
			.responseDecodable (decoder: decoder) { (response: DataResponse<T>) in
				completion(response.result)
		}
	}
	
	static func getTopHeadlines(parameters: Parameters, completion: @escaping (AFResult<News>)->Void) {
		request(route: APIRouter.topHeadlines(parameters: parameters), completion: completion)
	}
}
