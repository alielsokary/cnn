//
//  APIRouter.swift
//  CNN
//
//  Created by Ali on 8/27/19.
//  Copyright © 2019 mag. All rights reserved.
//

import Alamofire

enum APIRouter: URLRequestConvertible {
	
	case topHeadlines(parameters: Parameters)
	
	// MARK: - HTTPMethod
	private var method: HTTPMethod {
		switch self {
		case .topHeadlines:
			return .get
		}
	}
	
	// MARK: - Path
	private var path: String {
		switch self {
		case .topHeadlines:
			return "/top-headlines"
		}
	}
	
	// MARK: - Parameters
	private var parameters: Parameters? {
		switch self {
		case .topHeadlines(_):
			return nil
		}
	}
	
	// MARK: - URLRequestConvertible
	func asURLRequest() throws -> URLRequest {
		let url = try K.Production.baseURL.asURL()
		var urlRequest = URLRequest(url: url.appendingPathComponent(path))
		urlRequest.httpMethod = method.rawValue
		
		// Common Headers
		urlRequest.setValue(HTTPHeaderValueField.json.rawValue, forHTTPHeaderField: HTTPHeaderKeyField.acceptType.rawValue)
		urlRequest.setValue(HTTPHeaderValueField.json.rawValue, forHTTPHeaderField: HTTPHeaderKeyField.contentType.rawValue)
		urlRequest.setValue(HTTPHeaderValueField.APIKey.rawValue, forHTTPHeaderField: HTTPHeaderKeyField.XAPIToken.rawValue)
		
		switch self {
		case .topHeadlines(let parameters):
			urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
		}
		if let parameters = parameters {
			do {
				urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
			} catch {
				throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
			}
		}
		
		return urlRequest
	}
}
