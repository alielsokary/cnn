//
//  HelperMethods.swift
//  CNN
//
//  Created by Ali on 8/27/19.
//  Copyright © 2019 mag. All rights reserved.
//

import Foundation

class HelperMethods: NSObject {
	class func formatDate(date: String) -> String {
		var newDate = ""
		let dateFormatterGet = DateFormatter()
		dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
		
		let dateFormatter = DateFormatter()
		dateFormatter.dateStyle = .medium
		dateFormatter.timeStyle = .none		
		if let dateObj: Date = dateFormatterGet.date(from: date) {
			newDate = dateFormatter.string(from: dateObj)
		}
		return newDate
	}
}
