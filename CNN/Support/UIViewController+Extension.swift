//
//  UIViewController+Extension.swift
//  CNN
//
//  Created by Ali on 8/27/19.
//  Copyright © 2019 mag. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
	
	func presentAlert(withTitle title: String, message : String) {
		let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
		let OKAction = UIAlertAction(title: "OK", style: .default) { action in
		}
		alertController.addAction(OKAction)
		self.present(alertController, animated: true, completion: nil)
	}
}
