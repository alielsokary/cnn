//
//  RoundedView.swift
//  CNN
//
//  Created by Ali on 8/27/19.
//  Copyright © 2019 mag. All rights reserved.
//

import Foundation
import UIKit

class RoundedView: UIView {
	override var bounds: CGRect {
		didSet {
			setupRoundedView()
		}
	}
	
	private func setupRoundedView() {
		self.layer.cornerRadius = 8
		self.layer.masksToBounds = false
		self.layer.shadowOffset = CGSize(width: 0, height: 2)
		self.layer.shadowRadius = 2
		self.layer.shadowOpacity = 0.3
		self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 4, height: 4)).cgPath
		self.layer.shouldRasterize = true
		self.layer.rasterizationScale = UIScreen.main.scale
	}
}
