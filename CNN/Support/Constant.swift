//
//  Constant.swift
//  CNN
//
//  Created by Ali on 8/27/19.
//  Copyright © 2019 mag. All rights reserved.
//

import Foundation
import UIKit

struct K {
	struct Production {
		static let baseURL = "https://newsapi.org/v2"
	}
	
	struct APIParameterKey {
		static let country = "country"
		static let category = "category"
	}
	
	struct Colors {
		static let tintColor: UIColor = UIColor(red: 183.0/255.0, green: 0.0/255.0, blue: 4.0/255.0, alpha: 1.0)
	}
	
}

enum Countries: String {
	case usa = "us"
	case germany = "de"
	case france = "fr"
	case italy = "it"
	case russia = "ru"
}

enum AnalyticsKeys: String {
	case home_screen
	case tech_screen
	case sport_screen
	case news_details_screen
	
	case resource_link_clicked
}

enum Categories: String {
	case general = "general"
	case technology = "technology"
	case sports = "sports"
}
enum NotificationName: String {
	case mainTab = "MainTabSelected"
	case techTab = "TechTabSelected"
	case sportsTab = "SportsTabSelected"
}

enum HTTPHeaderKeyField: String {
	case acceptEncoding = "Accept-Encoding"
	case XAPIToken = "X-Api-Key"
	case acceptType = "Accept"
	case contentType = "Content-Type"
}

enum HTTPHeaderValueField: String {
	case APIKey = "b407a6f97850445fbbc17940a914b78a"
	case json = "application/json"
}
